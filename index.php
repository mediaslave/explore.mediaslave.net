<?php
include 'lib/helpers.php';
include 'lib/autoload.php';
use net\mediaslave\blogit\lib;

$article = '/home';
//Get the request request_uri
$request_uri = explode('.', $_SERVER['REQUEST_URI']);
$request_uri = $request_uri[0];
if($request_uri != '/'){
  $article = $request_uri;
}
$__title = ltrim($article, '/');
$__created_on = lib\createdOn($__title);
//Grab the content of the article and the layout.
ob_start();
  include 'articles' . $article . '.markdown';
  $__article = ob_get_contents();
ob_clean();
  include 'layouts/layout.html.php';

print ob_get_clean();
