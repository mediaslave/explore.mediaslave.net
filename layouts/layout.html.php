<?
use net\mediaslave\blogit\lib;
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <title>Blogit for PHP</title>
  <meta name="author" content="Justin Palmer">
</head>
<body>
  <header>
    <h1>Blog It</h1>
  </header>
  <div id="body" role="main">
    <?= lib\markdown($__article) ?>
    <?= lib\getLatestArticles(5) ?>
  </div>
  <div id="disqus_thread"></div>
  <footer>

  </footer>
  <script type="text/javascript">
    var disqus_shortname = 'info-slave'; // required: replace example with your forum shortname

    /* * * DON'T EDIT BELOW THIS LINE * * */
    (function() {
        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
        dsq.src = 'http://' + disqus_shortname + '.disqus.com/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
    })();
</script>
</body>
</html>
