<?php

function __autoload($class){

  $class = str_replace('net\mediaslave\blogit\\', '', $class);
  $class = str_replace('\\', '/', $class);
  include($class . '.php');
}
