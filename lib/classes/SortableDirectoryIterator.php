<?php
 namespace net\mediaslave\blogit\lib\classes;
/**
 * Sortable DirectoryIterator
 */
class SortableDirectoryIterator implements \IteratorAggregate {


  private $array = array();
  private $method = 'getFileName';
  private $doHidden = false;
  private $doReverse = false;

  /**
   *
   */
    public function __construct($path, $method="getFileName") {

      if (!is_array($path)) {
        $path = func_get_args();
      }
      $this->method($method);

      $this->sort($path);
    }

    public function method($method = 'getFileName') {
      $this->method = $method;
    }

    public function hidden($bool = true) {
      $this->doHidden = $bool;
    }

    public function reverse($bool = true){
      $this->doReverse = $bool;
    }

    public function getIterator() {
      if($this->doReverse){
        $this->array =  array_reverse($this->array);
      }
      return new \ArrayIterator($this->array);
    }

  /**
   * Sort the items by the correct method
   *
   * @DonaldKnuth
   *
   * @return void
   * @author Justin Palmer
   **/
  private function sort($args) {
    $method = $this->method;
    foreach($args as $path){
      if (!(is_dir($path))) {
          continue;
      }
      $i = new \DirectoryIterator($path);

      foreach($i as $File){
        if($this->doHidden == false && $File->isDot() || $File->isDir()){
          continue;
        }
        $key = $File->$method();
        $this->array[$key] = new \SplFileInfo($File->getPathName());
      }
    }
    ksort($this->array);
    $this->array = array_values($this->array);
  }
}
