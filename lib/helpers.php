<?php
namespace net\mediaslave\blogit\lib;

use net\mediaslave\blogit\lib\classes\Markdown;
use net\mediaslave\blogit\lib\classes\SortableDirectoryIterator;
use net\mediaslave\blogit\lib\classes\Inflections;
/**
 * Helper functions for the view.
 */

/**
 *
 * Turn the markdown into html
 *
 * @return string
 **/
function markdown($content){
  # Setup static parser variable.
  static $parser;
  if (!isset($parser)) {
    $parser = new Markdown;
  }

  # Transform text using parser.
  return $parser->transform($content);
}

/**
 *
 * Get the latest articles
 *
 * @return array
 * @author Justin Palmer
 **/
function getLatestArticleNames($num){
  $ret = array();
  $iterator = new SortableDirectoryIterator(__DIR__ . '/../articles', 'getMTime');
  $iterator->reverse();
  $ignore = array('home.markdown');
  foreach($iterator as $file){
    if(!in_array($file->getFileName(), $ignore)){
      $ret[] = array_shift(explode('.', $file->getFileName()));
      if(count($ret) == $num){
        break;
      }
    }
  }
  return $ret;
}



/**
 *
 * Get the latest articles
 *
 * @return array
 * @author Justin Palmer
 **/
function getLatestArticles($num){
  $ret = array();
  $iterator = new SortableDirectoryIterator(__DIR__ . '/../articles', 'getMTime');
  $iterator->reverse();
  $ignore = array('.', '..', 'home.markdown');
  foreach($iterator as $file){
    if(!in_array($file->getFileName(), $ignore)){
      $name =  array_shift(explode('.', $file->getFileName()));
      //Grab the content of the article and the layout.
      ob_start();
        include __DIR__ . '/../articles/' . $file->getFileName();
        $article = ob_get_contents();
      ob_clean();
      $ret[$name] = (object) array('article'=>$article,
                                   'created'=>$file->getMTime());
      if(count($ret) == $num){
        break;
      }
    }
  }
  return $ret;
}

/**
 *
 * Turn the filename into a title
 *
 * @param string $title
 * @return string
 * @author Justin Palmer
 **/
function titleize($title)
{
  return Inflections::titleize(implode(' ', explode('-', $title)));
}

/**
 *
 * Get the created on date
 *
 * @return string
 * @author Justin Palmer
 **/
function createdOn($file){
  $file = __DIR__ . '/../articles/' . $file . '.markdown';
  if(is_file($file)){
    $info = new \SplFileInfo($file);
    return $info->getMTime();
  }
  return 0;
}
